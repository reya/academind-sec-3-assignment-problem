import React from 'react';

const UserOutput = props => {
  const style = {
    backgroundColor: 'yellow',
    font: 'inherit',
    margin: '20px',
    fontWeight: 'bold',
    color: 'blue'
  };
  return (
    <div>
      <p style={style}>Username: {props.username}</p>
    </div>
  );
};

export default UserOutput;
