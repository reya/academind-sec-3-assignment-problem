import React from 'react';
import './User.css';

const UserInput = props => {
  return (
    <div>
      <span className="_title">UserName Input: </span>
      <input
        type="text"
        onChange={props.changed}
        value={props.username}
      ></input>
    </div>
  );
};

export default UserInput;
