# React - The complete Guide

## Section 3 - Assignment Problem

The instructions are:

- Create TWO new components: UserInput and UserOutput
- UserInput should hold an input element, UserOutput two paragraphs
- Output multiple UserOutput components in the App component (any paragraph texts of your choice)
- Pass a username (of your choice) to UserOutput via props and display it there
- Add state to the App component (=> the username) and pass the username to the UserOutput component
- Add a method to manipulate the state (=> an event-handler method)
- Pass the event-handler method reference to the UserInput component and bind it to the input-change event
- Ensure that the new input entered by the user overwrites the old username passed to UserOutput
- Add two-way-binding to your input (in UserInput) to also display the starting username
- Add styling of your choice to your components/ elements in the components - both with inline styles and stylesheets

## Author

Ariel Duarte

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
